// Directions:
// Listen to an event whena user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name inpuut on the left and the value of the last name input on the right

// Stretch goal: Instead of an anonymous function, create a new function that the two event listeners will call

// Guide question: Where do the names come from and where should they go?

inputOne = document.getElementById('txt-first-name');
inputTwo = document.getElementById('txt-last-name');
output = document.getElementById('span-full-name');


function generateOutput() {
	output.innerHTML = inputOne.value+inputTwo.value
}

inputOne.addEventListener('keyup', generateOutput);
inputTwo.addEventListener('keyup', generateOutput);

